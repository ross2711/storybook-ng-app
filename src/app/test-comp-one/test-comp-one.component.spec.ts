import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestCompOneComponent } from './test-comp-one.component';

describe('TestCompOneComponent', () => {
  let component: TestCompOneComponent;
  let fixture: ComponentFixture<TestCompOneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestCompOneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestCompOneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
