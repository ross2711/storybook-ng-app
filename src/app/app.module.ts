import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { TestCompOneComponent } from './test-comp-one/test-comp-one.component';
import { TestCompTwoComponent } from './test-comp-two/test-comp-two.component';

@NgModule({
  declarations: [
    AppComponent,
    TestCompOneComponent,
    TestCompTwoComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
